#define ISAMPERE
#define ISNVIDIA
// default screen resolution
#define SCRWIDTH	1600
#define SCRHEIGHT	1024

// IMPORTANT NOTE ON OPENCL COMPATIBILITY ON OLDER LAPTOPS:
// Without a GPU, a laptop needs at least a 'Broadwell' Intel CPU (5th gen, 2015):
// Intel's OpenCL implementation 'NEO' is not available on older devices.
// Same is true for Vulkan, OpenGL 4.0 and beyond, as well as DX11 and DX12.

// TODO:
// - If we have enough bricks for the full map, each brick pos can point to a fixed brick.
// - An irregular grid may be faster.
// - Add TAA and GI. :)

// settings shared between c/c++ and OpenCL
#define MAPWIDTH	1024	// total world size, x-axis
#define MAPHEIGHT	1024	// total world height
#define MAPDEPTH	1024	// total world size, z-axis
#define BRICKDIM	8		// brick dimensions
#define BDIMLOG2	3		// must be log2(BRICKDIM)
#define MAXCOMMITS	8192	// maximum number of bricks that can be committed per frame
#if 0
// 8-bit voxels: RGB332
#define VOXEL8
#define PAYLOAD unsigned char
#define PAYLOADSIZE	1
#else
// 16-bit voxels: MRGB4444, where M=material index
#define VOXEL16
#define PAYLOAD unsigned short
#define PAYLOADSIZE 2
#endif

// renderer performance setting: set to 0 for slower devices, up to 8 for fast GPUs
#define GIRAYS		6

// Panini projection, http://tksharpless.net/vedutismo/Pannini/panini.pdf via https://www.shadertoy.com/view/Wt3fzB
#define PANINI		0	// 0 to disable, 1 to enable

// TAA, as in https://www.shadertoy.com/view/3sfBWs
#define TAA			1	// 0 to disable, 1 to enable

// MSAA
#define AA_SAMPLES	1	// 1 to disable, 2..4 to enable. Note: will be squared.

// some useful color names
#ifdef VOXEL8
#define BLACK		(1<<5)	// actually: dark red; black itself is transparent
#define GREEN		(7<<2)
#define BLUE		3
#define RED			(7<<5)
#define YELLOW		(RED+GREEN)
#define WHITE		(255)
#define GREY		((3<<5)+(3<<2)+1)
#define ORANGE		((7<<5)+(5<<2))
#define LIGHTBLUE	(3+(4<<2)+(4<<5))
#define BROWN		((3<<5)+(1<<2))
#define LIGHTRED	(7<<5)+(2<<2)+1
#else
#define BLACK		0x001	// actually: dark blue; black itself is transparent
#define GREEN		0x0F0
#define BLUE		0x00F
#define RED			0xF00
#define YELLOW		0xFF0
#define WHITE		0xFFF
#define GREY		0x777
#define ORANGE		0xF70
#define LIGHTBLUE	0x77F
#define BROWN		0x720
#define LIGHTRED	0xF55
#endif

// renderer
struct RenderParams
{
	float2 oneOverRes;
	float3 E, p0, p1, p2;
	uint R0, frame;
	uint skyWidth, skyHeight;
	float4 skyLight[6];
	float skyLightScale, dummy1, dummy2, dummy3;
	// reprojection data
	float4 Nleft, Nright, Ntop, Nbottom;
	float4 prevRight, prevDown;
	float4 prevP0, prevP1, prevP2, prevP3;
};

// lighting for 6 normals for sky15.hdr
#define NX0			0.550f, 0.497f, 0.428f		// N = (-1, 0, 0)
#define NX1			0.399f, 0.352f, 0.299f		// N = ( 1, 0, 0)
#define NY0			0.470f, 0.428f, 0.373f		// N = ( 0,-1, 0)
#define NY1			0.370f, 0.346f, 0.312f		// N = ( 0, 1, 0)
#define NZ0			0.245f, 0.176f, 0.108f		// N = ( 0, 0,-1)
#define NZ1			0.499f, 0.542f, 0.564f		// N = ( 0, 0, 1)

// derived
#define GRIDWIDTH	(MAPWIDTH / BRICKDIM)
#define GRIDHEIGHT	(MAPHEIGHT / BRICKDIM)
#define GRIDDEPTH	(MAPDEPTH / BRICKDIM)
#define GRIDSIZE	(GRIDWIDTH * GRIDHEIGHT * GRIDWIDTH)
#define BRICKSIZE	(BRICKDIM * BRICKDIM * BRICKDIM)
#define UBERWIDTH	(GRIDWIDTH / 4)
#define UBERHEIGHT	(GRIDHEIGHT / 4)
#define UBERDEPTH	(GRIDDEPTH / 4)
// note: we reserve 50% of the theoretical peak; a normal scene shouldn't come close to
// using that many unique (non-empty!) bricks.
#define BRICKCOUNT	((((MAPWIDTH / BRICKDIM) * (MAPHEIGHT / BRICKDIM) * (MAPDEPTH / BRICKDIM))) / 2)
#define BRICKCOMMITSIZE	(MAXCOMMITS * BRICKSIZE * PAYLOADSIZE + MAXCOMMITS * 4 /* bytes */)
#define CHUNKCOUNT	4
#define CHUNKSIZE	((BRICKCOUNT * BRICKSIZE * PAYLOADSIZE) / CHUNKCOUNT)

// experimental
#define ONEBRICKBUFFER	1 // use a single (large) brick buffer; set to 0 on low mem devices
#define MORTONBRICKS	0 // store bricks in morton order to improve data locality (slower)

// constants
#define PI			3.14159265358979323846264f
#define INVPI		0.31830988618379067153777f
#define INV2PI		0.15915494309189533576888f
#define TWOPI		6.28318530717958647692528f
#define SQRT_PI_INV	0.56418958355f
#define LARGE_FLOAT	1e34f
// Optimizations / plans:
// 1. Automate finding the optimal workgroup size
// 2. Keep trying with fewer registers
// 3. Try a 1D job and turn it into tiles in the render kernel
// 4. If all threads enter the same brick, this brick can be in local mem
// 5. Create a website with a library of vox files
// 6. Have a nicer benchmark scene: planet with asteroid debris?
// 7. Trace does not respect initial t
// 8. Add occlusion ray query

// DONE:
// - Figure out how to detect 1080/2080/3080/AMD/other
// - Optimize for 2080 and 1080
// - Try some unrolling on the 2nd loop?
// - Optimize the world: combine 8x8x8 solid voxels
// - Improve readme.md
// - Add UI to obj2vox


// internal stuff
#define OFFS_X		((bits >> 5) & 1)			// extract grid plane offset over x (0 or 1)
#define OFFS_Y		((bits >> 13) & 1)			// extract grid plane offset over y (0 or 1)
#define OFFS_Z		(bits >> 21)				// extract grid plane offset over z (0 or 1)
#define DIR_X		((bits & 3) - 1)			// ray dir over x (-1 or 1)
#define DIR_Y		(((bits >> 8) & 3) - 1)		// ray dir over y (-1 or 1)
#define DIR_Z		(((bits >> 16) & 3) - 1)	// ray dir over z (-1 or 1)
#define EPS			1e-8
#define BMSK		(BRICKDIM - 1)
#define BDIM2		(BRICKDIM * BRICKDIM)
#define BPMX		(MAPWIDTH - BRICKDIM)
#define BPMY		(MAPHEIGHT - BRICKDIM)
#define BPMZ		(MAPDEPTH - BRICKDIM)
#define TOPMASK3	(((1023 - BMSK) << 20) + ((1023 - BMSK) << 10) + (1023 - BMSK))
#define UBERMASK3	((1020 << 20) + (1020 << 10) + 1020)

// fix ray directions that are too close to 0
float4 FixZeroDeltas( float4 V )
{
	if (fabs( V.x ) < EPS) V.x = V.x < 0 ? -EPS : EPS;
	if (fabs( V.y ) < EPS) V.y = V.y < 0 ? -EPS : EPS;
	if (fabs( V.z ) < EPS) V.z = V.z < 0 ? -EPS : EPS;
	return V;
}

#if ONEBRICKBUFFER == 1

#define BRICKSTEP(exitLabel)															\
	v = o + (p >> 20) + ((p >> 7) & (BMSK * BRICKDIM)) + (p & BMSK) * BDIM2;			\
	v = brick0[v]; if (v) { *dist = t + to, * side = last; return v; }					\
	t = min( tm.x, min( tm.y, tm.z ) ), last = 0;										\
	if (t == tm.x) tm.x += td.x, p += dx;												\
	if (t == tm.y) tm.y += td.y, p += dy, last = 1;										\
	if (t == tm.z) tm.z += td.z, p += dz, last = 2;										\
	if (p & TOPMASK3) goto exitLabel;

#else

#define BRICKSTEP(exitLabel)															\
	v = o + (p >> 20) + ((p >> 7) & (BMSK * BRICKDIM)) + (p & BMSK) * BDIM2;			\
	if (p != lp) page = (__global const PAYLOAD*)bricks[v / (CHUNKSIZE / PAYLOADSIZE)], lp = p;	\
	v = page[v & ((CHUNKSIZE / PAYLOADSIZE) - 1)];										\
	if (v) { *dist = t + to, * side = last; return v; }									\
	t = min( tm.x, min( tm.y, tm.z ) ), last = 0;										\
	if (t == tm.x) tm.x += td.x, p += dx;												\
	if (t == tm.y) tm.y += td.y, p += dy, last = 1;										\
	if (t == tm.z) tm.z += td.z, p += dz, last = 2;										\
	if (p & TOPMASK3) goto exitLabel;

#endif

#define GRIDSTEP(exitX)																			\
	if (!--steps) break;																		\
	if (o != 0) if (!(o & 1)) { *dist = (t + to) * 8.0f, *side = last; return o >> 1; } else	\
	{																							\
		const float4 tm_ = tm;																	\
		const uint4 p4 = convert_uint4( A + V * (t *= 8) );										\
		uint v, p = (clamp( p4.x, tp >> 17, (tp >> 17) + 7 ) << 20) +							\
			(clamp( p4.y, (tp >> 7) & 1023, ((tp >> 7) & 1023) + 7 ) << 10) +					\
			clamp( p4.z, (tp << 3) & 1023, ((tp << 3) & 1023) + 7 ), lp = ~1;					\
		tm = (convert_float4( (uint4)((p >> 20) + OFFS_X, ((p >> 10) & 1023) +					\
			OFFS_Y, (p & 1023) + OFFS_Z, 0) ) - A) * rV;										\
		p &= 7 + (7 << 10) + (7 << 20), o = --o << 8;											\
		BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX );			\
		BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX );			\
		BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX );			\
		BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX );			\
		BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX );			\
		BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX ); BRICKSTEP( exitX );			\
	exitX: tm = tm_;																			\
	}																							\
	t = min( tm.x, min( tm.y, tm.z ) ), last = 0;												\
	if (t == tm.x) tm.x += td.x, tp += dx;														\
	if (t == tm.y) tm.y += td.y, tp += dy, last = 1;											\
	if (t == tm.z) tm.z += td.z, tp += dz, last = 2;											\
	if ((tp & UBERMASK3) - tq) break;															\
	o = read_imageui( grid, (int4)(tp >> 20, tp & 127, (tp >> 10) & 127, 0) ).x;

// mighty two-level grid traversal
uint TraceRay( float4 A, const float4 B, float* dist, uint* side, __read_only image3d_t grid,
	__global const unsigned char* uberGrid,
	__global const PAYLOAD* brick0,
#if ONEBRICKBUFFER == 0
	__global const PAYLOAD* brick1,
	__global const PAYLOAD* brick2,
	__global const PAYLOAD* brick3,
#endif
	int steps
)
{
#if ONEBRICKBUFFER == 0
	__global const PAYLOAD* bricks[4] = { brick0, brick1, brick2, brick3 };
#endif
	const float4 V = FixZeroDeltas( B ), rV = (float4)(1.0 / V.x, 1.0 / V.y, 1.0 / V.z, 1);
	float to = 0; // distance to travel to get into grid
	const int bits = select( 4, 34, V.x > 0 ) + select( 3072, 10752, V.y > 0 ) + select( 1310720, 3276800, V.z > 0 ); // magic
	uint last = 0, dx = DIR_X << 20, dy = DIR_Y << 10, dz = DIR_Z;
	if (A.x < 0 || A.y < 0 || A.z < 0 || A.x > MAPWIDTH || A.y > MAPHEIGHT || A.z > MAPDEPTH)
	{
		// use slab test to clip ray origin against scene AABB
		const float tx1 = -A.x * rV.x, tx2 = (MAPWIDTH - A.x) * rV.x;
		float tmin = min( tx1, tx2 ), tmax = max( tx1, tx2 );
		const float ty1 = -A.y * rV.y, ty2 = (MAPHEIGHT - A.y) * rV.y;
		tmin = max( tmin, min( ty1, ty2 ) ), tmax = min( tmax, max( ty1, ty2 ) );
		const float tz1 = -A.z * rV.z, tz2 = (MAPDEPTH - A.z) * rV.z;
		tmin = max( tmin, min( tz1, tz2 ) ), tmax = min( tmax, max( tz1, tz2 ) );
		if (tmax < tmin || tmax <= 0) return 0; // ray misses scene 
		A += tmin * V, to = tmin; // new ray entry point
		// update 'last', for correct handling of hits on the border of the map
		if (A.y < 0.01f || A.y >( MAPHEIGHT - 1.01f )) last = 1;
		if (A.z < 0.01f || A.z >( MAPDEPTH - 1.01f )) last = 2;
	}
	uint up = (clamp( (uint)A.x >> 5, 0u, 31u ) << 20) + (clamp( (uint)A.y >> 5, 0u, 31u ) << 10) +
		clamp( (uint)A.z >> 5, 0u, 31u );
	float4 tm = ((float4)((up >> 20) + OFFS_X, ((up >> 10) & 31) + OFFS_Y, (up & 31) + OFFS_Z, 0) - A * 0.03125f) * rV;
	float t = 0;
	const float4 td = (float4)(DIR_X, DIR_Y, DIR_Z, 0) * rV;
	uint o = uberGrid[(up >> 20) + ((up & 31) << 5) + (((up >> 10) & 31) << 10)];
	while (1)
	{
		if ((steps -= 4) <= 0) break;
		if (o)
		{
			// backup ubergrid traversal state
			const float4 tm_ = tm;
			// intialize topgrid traversal
			const uint4 p4 = convert_uint4( 0.125f * A + V * (t *= 4) );
			uint tp = (clamp( p4.x, up >> 18, (up >> 18) + 3 ) << 20) +
				(clamp( p4.y, (up >> 8) & 1023, ((up >> 8) & 1023) + 3 ) << 10) +
				clamp( p4.z, (up << 2) & 1023, ((up << 2) & 1023) + 3 ), tq = tp & UBERMASK3;
			tm = (convert_float4( (uint4)((tp >> 20) + OFFS_X, ((tp >> 10) & 127) + OFFS_Y,
				(tp & 127) + OFFS_Z, 0) ) - A * 0.125f) * rV;
			o = read_imageui( grid, (int4)(tp >> 20, tp & 127, (tp >> 10) & 127, 0) ).x;
			while (1)
			{
			#if ONEBRICKBUFFER == 0
				__global const PAYLOAD* page;
			#endif
			#ifdef ISAMPERE
				GRIDSTEP(exit1); GRIDSTEP(exit2); // Ampere has massive L1I$;
				GRIDSTEP(exit3); GRIDSTEP(exit4); // unroll 4x for best performance.
			#else
				GRIDSTEP(exit1); // Turing and older have smaller L1I$, don't unroll
			#endif
			}
			// restore ubergrid traversal state
			tm = tm_;
		}
		t = min( tm.x, min( tm.y, tm.z ) ), last = 0;
		if (t == tm.x) tm.x += td.x, up += dx;
		if (t == tm.y) tm.y += td.y, up += dy, last = 1;
		if (t == tm.z) tm.z += td.z, up += dz, last = 2;
		if (up & 0xfe0f83e0) break;
		o = uberGrid[(up >> 20) + ((up & 31) << 5) + (((up >> 10) & 31) << 10)];
	}
	return 0U;
}

void TraceRayToVoid( float4 A, float4 V, float* dist, float3* N, __read_only image3d_t grid,
#if ONEBRICKBUFFER == 1
	__global const PAYLOAD* brick,
#else
	__global const PAYLOAD* brick0, __global const PAYLOAD* brick1,
	__global const PAYLOAD* brick2, __global const PAYLOAD* brick3,
#endif
	__global const unsigned char* uber
)
{
#if 0 // TODO
#if ONEBRICKBUFFER == 0
	__global const PAYLOAD* bricks[4] = { brick0, brick1, brick2, brick3 };
#endif
	V = FixZeroDeltas( V );
	const float4 rV = (float4)(1.0 / V.x, 1.0 / V.y, 1.0 / V.z, 1);
	if (A.x < 0 || A.y < 0 || A.z < 0 || A.x > MAPWIDTH || A.y > MAPHEIGHT || A.z > MAPDEPTH)
	{
		*dist = 0; // we start outside the grid, and thus in empty space: don't do that
		return;
	}
	uint tp = (clamp( (uint)A.x >> 3, 0u, 127u ) << 20) + (clamp( (uint)A.y >> 3, 0u, 127u ) << 10) +
		clamp( (uint)A.z >> 3, 0u, 127u );
	const int bits = select( 4, 34, V.x > 0 ) + select( 3072, 10752, V.y > 0 ) + select( 1310720, 3276800, V.z > 0 ); // magic
	float4 tm = ((float4)(((tp >> 20) & 127) + ((bits >> 5) & 1), ((tp >> 10) & 127) + ((bits >> 13) & 1),
		(tp & 127) + ((bits >> 21) & 1), 0) - A * 0.125f) * rV;
	float4 td = (float4)(DIR_X, DIR_Y, DIR_Z, 0) * rV;
	float t = 0;
	uint last = 0;
	do
	{
		// fetch brick from top grid
		uint o = read_imageui( grid, (int4)(tp >> 20, tp & 127, (tp >> 10) & 127, 0) ).x;
		if (o == 0) /* empty brick: done */
		{
			*dist = t * 8.0f;
			*N = -(float3)((last == 0) * DIR_X, (last == 1) * DIR_Y, (last == 2) * DIR_Z);
			return;
		}
		else if ((o & 1) == 1) /* non-empty brick */
		{
			// backup top-grid traversal state
			const float4 tm_ = tm;
			// intialize brick traversal
			tm = A + V * (t *= 8); // abusing tm for I to save registers
			uint p = (clamp( (uint)tm.x, tp >> 17, (tp >> 17) + 7 ) << 20) +
				(clamp( (uint)tm.y, (tp >> 7) & 1023, ((tp >> 7) & 1023) + 7 ) << 10) +
				clamp( (uint)tm.z, (tp << 3) & 1023, ((tp << 3) & 1023) + 7 ), lp = ~1;
			tm = ((float4)((p >> 20) + OFFS_X, ((p >> 10) & 1023) + OFFS_Y, (p & 1023) + OFFS_Z, 0) - A) * rV;
			p &= 7 + (7 << 10) + (7 << 20), o = (o >> 1) * BRICKSIZE;
			__global const PAYLOAD* page;
			do // traverse brick
			{
				o += (p >> 20) + ((p >> 7) & (BMSK * BRICKDIM)) + (p & BMSK) * BDIM2;
			#if ONEBRICKBUFFER == 1
				if (!((__global const PAYLOAD*)brick0)[o])
				#else
				if (p != lp) page = (__global const PAYLOAD*)bricks[o / (CHUNKSIZE / PAYLOADSIZE)], lp = p;
				if (!page[o & ((CHUNKSIZE / PAYLOADSIZE) - 1)])
				#endif
				{
					*dist = t;
					*N = -(float3)((last == 0) * DIR_X, (last == 1) * DIR_Y, (last == 2) * DIR_Z);
					return;
				}
				t = min( tm.x, min( tm.y, tm.z ) );
				if (t == tm.x) tm.x += td.x, p += DIR_X << 20, last = 0;
				else if (t == tm.y) tm.y += td.y, p += ((bits << 2) & 3072) - 1024, last = 1;
				else if (t == tm.z) tm.z += td.z, p += DIR_Z, last = 2;
			} while (!(p & TOPMASK3));
			tm = tm_; // restore top-grid traversal state
		}
		t = min( tm.x, min( tm.y, tm.z ) );
		if (t == tm.x) tm.x += td.x, tp += DIR_X << 20, last = 0;
		else if (t == tm.y) tm.y += td.y, tp += DIR_Y << 10, last = 1;
		else if (t == tm.z) tm.z += td.z, tp += DIR_Z, last = 2;
	} while (!(tp & 0xf80e0380));
#endif
}
float SphericalTheta( const float3 v )
{
	return acos( clamp( v.z, -1.f, 1.f ) );
}

float SphericalPhi( const float3 v )
{
	const float p = atan2( v.y, v.x );
	return (p < 0) ? (p + 2 * PI) : p;
}

uint WangHash( uint s ) { s = (s ^ 61) ^ (s >> 16), s *= 9, s = s ^ (s >> 4), s *= 0x27d4eb2d, s = s ^ (s >> 15); return s; }
uint RandomInt( uint* s ) { *s ^= *s << 13, * s ^= *s >> 17, * s ^= *s << 5; return *s; }
float RandomFloat( uint* s ) { return RandomInt( s ) * 2.3283064365387e-10f; }

float3 DiffuseReflectionCosWeighted( const float r0, const float r1, const float3 N )
{
	const float3 T = normalize( cross( N, fabs( N.y ) > 0.99f ? (float3)(1, 0, 0) : (float3)(0, 1, 0) ) );
	const float3 B = cross( T, N );
	const float term1 = TWOPI * r0, term2 = sqrt( 1 - r1 );
	float c, s = sincos( term1, &c );
	return (c * term2 * T) + (s * term2) * B + sqrt( r1 ) * N;
}

float blueNoiseSampler( const __global uint* blueNoise, int x, int y, int sampleIndex, int sampleDimension )
{
	// Adapated from E. Heitz. Arguments:
	// sampleIndex: 0..255
	// sampleDimension: 0..255
	x &= 127, y &= 127, sampleIndex &= 255, sampleDimension &= 255;
	// xor index based on optimized ranking
	int rankedSampleIndex = (sampleIndex ^ blueNoise[sampleDimension + (x + y * 128) * 8 + 65536 * 3]) & 255;
	// fetch value in sequence
	int value = blueNoise[sampleDimension + rankedSampleIndex * 256];
	// if the dimension is optimized, xor sequence value based on optimized scrambling
	value ^= blueNoise[(sampleDimension & 7) + (x + y * 128) * 8 + 65536];
	// convert to float and return
	float retVal = (0.5f + value) * (1.0f / 256.0f) /* + noiseShift (see LH2) */;
	if (retVal >= 1) retVal -= 1;
	return retVal;
}

// tc ∈ [-1,1]² | fov ∈ [0, π) | d ∈ [0,1] -  via https://www.shadertoy.com/view/tt3BRS
float3 PaniniProjection( float2 tc, const float fov, const float d )
{
	const float d2 = d * d;
	{
		const float fo = PI * 0.5f - fov * 0.5f;
		const float f = cos( fo ) / sin( fo ) * 2.0f;
		const float f2 = f * f;
		const float b = (native_sqrt( max( 0.f, (d + d2) * (d + d2) * (f2 + f2 * f2) ) ) - (d * f + f)) / (d2 + d2 * f2 - 1);
		tc *= b;
	}
	const float h = tc.x, v = tc.y, h2 = h * h;
	const float k = h2 / ((d + 1) * (d + 1)), k2 = k * k;
	const float discr = max( 0.f, k2 * d2 - (k + 1) * (k * d2 - 1) );
	const float cosPhi = (-k * d + native_sqrt( discr )) / (k + 1.f);
	const float S = (d + 1) / (d + cosPhi), tanTheta = v / S;
	float sinPhi = native_sqrt( max( 0.f, 1 - cosPhi * cosPhi ) );
	if (tc.x < 0.0) sinPhi *= -1;
	const float s = native_rsqrt( 1 + tanTheta * tanTheta );
	return (float3)(sinPhi, tanTheta, cosPhi) * s;
}

__constant float halton[32] = { 
	0, 0, 0.5f, 0.333333f, 0, 0.6666666f, 0.75f, 0.111111111f, 0, 0.44444444f, 
	0.5f, 0.7777777f, 0.25f, 0.222222222f, 0.75f, 0.55555555f, 0, 0.88888888f, 
	0.5f, 0.03703703f, 0.25f, 0.37037037f, 0.75f, 0.70370370f, 0.125f, 0.148148148f,
	0.625f, 0.481481481f, 0.375f, 0.814814814f, 0.875f, 0.259259259f 
};

// produce a camera ray direction for a position in screen space
float3 GenerateCameraRay( const float2 pixelPos, __constant struct RenderParams* params )
{
#if TAA
	const uint px = (uint)pixelPos.x;
	const uint py = (uint)pixelPos.y;
	const uint h = (params->frame + (px & 3) + 4 * (py & 3)) & 15;
	const float2 uv = (float2)(
		(pixelPos.x + halton[h * 2 + 0]) * params->oneOverRes.x, 
		(pixelPos.y + halton[h * 2 + 1]) * params->oneOverRes.y
	);
#else
	const float2 uv = (float2)(
		pixelPos.x * params->oneOverRes.x, 
		pixelPos.y * params->oneOverRes.y
	);
#endif
#if PANINI
	const float3 V = PaniniProjection( (float2)(uv.x * 2 - 1, (uv.y * 2 - 1) * ((float)SCRHEIGHT / SCRWIDTH)), PI / 5, 0.15f );
	// multiply by improvised camera matrix
	return V.z * normalize( (params->p1 + params->p2) * 0.5f - params->E ) +
		V.x * normalize( params->p1 - params->p0 ) + V.y * normalize( params->p2 - params->p0 );
#else
	const float3 P = params->p0 + (params->p1 - params->p0) * uv.x + (params->p2 - params->p0) * uv.y;
	return normalize( P - params->E );
#endif
}

// sample the HDR sky dome texture (bilinear)
float3 SampleSky( const float3 T, __global float4* sky, uint w, uint h )
{
	const float u = w * SphericalPhi( T ) * INV2PI - 0.5f;
	const float v = h * SphericalTheta( T ) * INVPI - 0.5f;
	const float fu = u - floor( u ), fv = v - floor( v );
	const int iu = (int)u, iv = (int)v;
	const uint idx1 = (iu + iv * w) % (w * h);
	const uint idx2 = (iu + 1 + iv * w) % (w * h);
	const uint idx3 = (iu + (iv + 1) * w) % (w * h);
	const uint idx4 = (iu + 1 + (iv + 1) * w) % (w * h);
	const float4 s =
		sky[idx1] * (1 - fu) * (1 - fv) + sky[idx2] * fu * (1 - fv) +
		sky[idx3] * (1 - fu) * fv + sky[idx4] * fu * fv;
	return s.xyz;
}

// convert a voxel color to floating point rgb
float3 ToFloatRGB( const uint v )
{
#if PAYLOADSIZE == 1
	return (float3)((v >> 5) * (1.0f / 7.0f), ((v >> 2) & 7) * (1.0f / 7.0f), (v & 3) * (1.0f / 3.0f));
#else
	return (float3)(((v >> 8) & 15) * (1.0f / 15.0f), ((v >> 4) & 15) * (1.0f / 15.0f), (v & 15) * (1.0f / 15.0f));
#endif
}

// ACES filmic tonemapping, via https://www.shadertoy.com/view/3sfBWs
float3 ACESFilm( const float3 x )
{
	float a = 2.51f, b = 0.03f, c = 2.43f, d = 0.59f, e = 0.14f;
	return clamp( (x * (a * x + b)) / (x * (c * x + d) + e), 0.0f, 1.0f );
}

// Reinhard2 tonemapping, via https://www.shadertoy.com/view/WdjSW3
float3 Reinhard2( const float3 x )
{
	const float3 L_white = (float3)4.0f;
	return (x * (1.0f + x / (L_white * L_white))) / (1.0f + x);
}

// https://twitter.com/jimhejl/status/633777619998130176
float3 ToneMapFilmic_Hejl2015(const float3 hdr, float whitePt) 
{
    float4 vh = (float4)(hdr, whitePt);
    float4 va = 1.425f * vh + 0.05f;
    float4 vf = (vh * va + 0.004f) / (vh * (va + 0.55f) + 0.0491f) - 0.0821f;
    return vf.xyz / vf.www;
}

// Linear to SRGB, also via https://www.shadertoy.com/view/3sfBWs
float3 LessThan( const float3 f, float value )
{
	return (float3)(
		(f.x < value) ? 1.0f : 0.0f,
		(f.y < value) ? 1.0f : 0.0f,
		(f.z < value) ? 1.0f : 0.0f);
}
float3 LinearToSRGB( const float3 rgb )
{
#if 0
	return sqrt( rgb );
#else
	const float3 _rgb = clamp( rgb, 0.0f, 1.0f );
	return mix( pow( _rgb * 1.055f, (float3)(1.f / 2.4f) ) - 0.055f,
		_rgb * 12.92f, LessThan( _rgb, 0.0031308f )
	);
#endif
}

// conversions between RGB and YCoCG for TAA
float3 RGBToYCoCg( const float3 RGB )
{
	const float3 rgb = min( (float3)( 4 ), RGB ); // clamp helps AA for strong HDR
	const float Y = dot( rgb, (float3)( 1, 2, 1 ) ) * 0.25f;
	const float Co = dot( rgb, (float3)( 2, 0, -2 ) ) * 0.25f + (0.5f * 256.0f / 255.0f);
	const float Cg = dot( rgb, (float3)( -1, 2, -1 ) ) * 0.25f + (0.5f * 256.0f / 255.0f);
	return (float3)( Y, Co, Cg );
}

float3 YCoCgToRGB( const float3 YCoCg )
{
	const float Y = YCoCg.x;
	const float Co = YCoCg.y - (0.5f * 256.0f / 255.0f);
	const float Cg = YCoCg.z - (0.5f * 256.0f / 255.0f);
	return (float3)( Y + Co - Cg, Y + Cg, Y - Co - Cg );
}

// sample a color buffer with bilinear interpolation
float3 bilerpSample( const __global float4* buffer, const float u, const float v )
{
	// do a bilerp fetch at u_prev, v_prev
	float fu = u - floor( u ), fv = v - floor( v );
	int iu = (int)u, iv = (int)v;
	if (iu == 0 || iv == 0 || iu >= SCRWIDTH - 1 || iv >= SCRHEIGHT - 1) return (float3)0;
	int iu0 = clamp( iu, 0, SCRWIDTH - 1 ), iu1 = clamp( iu + 1, 0, SCRWIDTH - 1 );
	int iv0 = clamp( iv, 0, SCRHEIGHT - 1 ), iv1 = clamp( iv + 1, 0, SCRHEIGHT - 1 );
	float4 p0 = (1 - fu) * (1 - fv) * buffer[iu0 + iv0 * SCRWIDTH];
	float4 p1 = fu * (1 - fv) * buffer[iu1 + iv0 * SCRWIDTH];
	float4 p2 = (1 - fu) * fv * buffer[iu0 + iv1 * SCRWIDTH];
	float4 p3 = fu * fv * buffer[iu1 + iv1 * SCRWIDTH];
	return (p0 + p1 + p2 + p3).xyz;
}

// inefficient morton code for bricks (3 bit per axis, x / y / z)
int Morton3Bit( const int x, const int y, const int z )
{	
	return (x & 1) + 2 * (y & 1) + 4 * (z & 1) +
		4 * (x & 2) + 8 * (y & 2) + 16 * (z & 2) +
		16 * (x & 4) + 32 * (y & 4) + 64 * (z & 4);
}
int Morton3( const int xyz )
{	
	return (xyz & 1) + ((xyz & 8) >> 2) + ((xyz & 64) >> 4) +
		((xyz & 2) << 2) + (xyz & 16) + ((xyz & 128) >> 2) + 
		((xyz & 4) << 4) + ((xyz & 32) << 2) + (xyz & 256);
}

// building a normal from an axis and a ray direction
float3 VoxelNormal( const uint side, const float3 D )
{
	if (side == 0) return (float3)(D.x > 0 ? -1 : 1, 0, 0 );
	if (side == 1) return (float3)(0, D.y > 0 ? -1 : 1, 0 );
	if (side == 2) return (float3)(0, 0, D.z > 0 ? -1 : 1 );
}

#if ONEBRICKBUFFER == 1
#define BRICKPARAMS brick0
#else
#define BRICKPARAMS brick0, brick1, brick2, brick3
#endif

float4 render_whitted( const float2 screenPos, __constant struct RenderParams* params,
	__read_only image3d_t grid,
	__global const PAYLOAD* brick0,
#if ONEBRICKBUFFER == 0
	__global const PAYLOAD* brick1, __global const PAYLOAD* brick2, __global const PAYLOAD* brick3,
#endif
	__global float4* sky, __global const uint* blueNoise,
	__global const unsigned char* uberGrid
)
{
	// basic AA
	float3 pixel = (float3)(0);
	float dist; // TAA will use distance of the last sample; it should be the only one.
	for (int u = 0; u < AA_SAMPLES; u++) for (int v = 0; v < AA_SAMPLES; v++)
	{
		// trace primary ray
		uint side = 0;
		const float3 D = GenerateCameraRay( screenPos + (float2)((float)u * (1.0f / AA_SAMPLES), (float)v * (1.0f / AA_SAMPLES)), params );
		const uint voxel = TraceRay( (float4)(params->E, 0), (float4)(D, 1), &dist, &side, grid, uberGrid, BRICKPARAMS, 999999 /* no cap needed */ );
		// simple hardcoded directional lighting using arbitrary unit vector
		if (voxel == 0) return (float4)(SampleSky( (float3)(D.x, D.z, D.y), sky, params->skyWidth, params->skyHeight ), 1e20f);
		{	// scope limiting
			const float3 BRDF1 = INVPI * ToFloatRGB( voxel );
			float4 sky;
			if (side == 0) sky = params->skyLight[D.x > 0 ? 0 : 1];
			if (side == 1) sky = params->skyLight[D.y > 0 ? 2 : 3];
			if (side == 2) sky = params->skyLight[D.z > 0 ? 4 : 5];
			pixel += BRDF1 * params->skyLightScale * sky.xyz;
		}
	}
	return (float4)(pixel * (1.0f / (AA_SAMPLES * AA_SAMPLES)), dist);
}

float4 render_gi( const float2 screenPos, __constant struct RenderParams* params,
	__read_only image3d_t grid,
	__global const PAYLOAD* brick0,
#if ONEBRICKBUFFER == 0
	__global const PAYLOAD* brick1, __global const PAYLOAD* brick2, __global const PAYLOAD* brick3,
#endif
	__global float4* sky, __global const uint* blueNoise,
	__global const unsigned char* uberGrid
)
{
	// trace primary ray
	float dist;
	uint side = 0;
	const float3 D = GenerateCameraRay( screenPos, params );
	const uint voxel = TraceRay( (float4)(params->E, 0), (float4)(D, 1), &dist, &side, grid, uberGrid, BRICKPARAMS, 999999 /* no cap needed */ );
	const float skyLightScale = params->skyLightScale;
	// visualize result: simple hardcoded directional lighting using arbitrary unit vector
	if (voxel == 0) return (float4)(SampleSky( (float3)(D.x, D.z, D.y), sky, params->skyWidth, params->skyHeight ), 1e20f);
	const float3 BRDF1 = INVPI * ToFloatRGB( voxel );
	float3 incoming = (float3)(0, 0, 0);
	const int x = (int)screenPos.x, y = (int)screenPos.y;
	uint seed = WangHash( x * 171 + y * 1773 + params->R0 );
	const float4 I = (float4)(params->E + D * dist, 0);
	for (int i = 0; i < GIRAYS; i++)
	{
		const float r0 = blueNoiseSampler( blueNoise, x, y, i + GIRAYS * params->frame, 0 );
		const float r1 = blueNoiseSampler( blueNoise, x, y, i + GIRAYS * params->frame, 1 );
		const float3 N = VoxelNormal( side, D );
		const float4 R = (float4)(DiffuseReflectionCosWeighted( r0, r1, N ), 1);
		uint side2;
		float dist2;
		const uint voxel2 = TraceRay( I + 0.1f * (float4)(N, 0), R, &dist2, &side2, grid, uberGrid, BRICKPARAMS, GRIDWIDTH / 12 );
		const float3 N2 = VoxelNormal( side2, R.xyz );
		if (0 /* for comparing against ground truth */) // get_global_id( 0 ) % SCRWIDTH < SCRWIDTH / 2)
		{
			if (voxel2 == 0) incoming += skyLightScale * SampleSky( (float3)(R.x, R.z, R.y), sky, params->skyWidth, params->skyHeight ); else /* secondary hit */
			{
				const float4 R2 = (float4)(DiffuseReflectionCosWeighted( r0, r1, N2 ), 1);
				incoming += INVPI * ToFloatRGB( voxel2 ) * skyLightScale * SampleSky( (float3)(R2.x, R2.z, R2.y), sky, params->skyWidth, params->skyHeight );
			}
		}
		else
		{
			float3 toAdd = (float3)skyLightScale, M = N;
			if (voxel2 != 0) toAdd *= INVPI * ToFloatRGB( voxel2 ), M = N2;
			float4 sky;
			if (M.x < -0.9f) sky = params->skyLight[0];
			if (M.x > 0.9f) sky = params->skyLight[1];
			if (M.y < -0.9f) sky = params->skyLight[2];
			if (M.y > 0.9f) sky = params->skyLight[3];
			if (M.z < -0.9f) sky = params->skyLight[4];
			if (M.z > 0.9f) sky = params->skyLight[5];
			incoming += toAdd * sky.xyz;
		}
	}
	return (float4)(BRDF1 * incoming * (1.0f / GIRAYS), dist);
}

// renderTAA: main rendering entry point. Forwards the request to either a basic
// renderer or a path tracer with basic indirect light. 'NoTAA' version below.
__kernel void renderTAA( __global float4* frame, __constant struct RenderParams* params,
	__read_only image3d_t grid, __global float4* sky, __global const uint* blueNoise,
	__global const unsigned char* uberGrid, __global const PAYLOAD* brick0
#if ONEBRICKBUFFER == 0
	, __global const PAYLOAD* brick1, __global const PAYLOAD* brick2, __global const PAYLOAD* brick3
#endif
)
{
	// produce primary ray for pixel
	const int x = get_global_id( 0 ), y = get_global_id( 1 );
#if GIRAYS == 0
	float4 pixel = render_whitted( (float2)(x, y), params, grid, BRICKPARAMS, sky, blueNoise, uberGrid );
#else
	float4 pixel = render_gi( (float2)(x, y), params, grid, BRICKPARAMS, sky, blueNoise, uberGrid );
#endif
	// store pixel in linear color space, to be processed by finalize kernel for TAA
	frame[x + y * SCRWIDTH] = pixel; // depth in w
}

// renderNoTAA: main rendering entry point. Forwards the request to either a basic
// renderer or a path tracer with basic indirect light. Version without TAA.
__kernel void renderNoTAA( write_only image2d_t outimg, __constant struct RenderParams* params,
	__read_only image3d_t grid, __global float4* sky, __global const uint* blueNoise,
	__global const unsigned char* uberGrid, __global const PAYLOAD* brick0
#if ONEBRICKBUFFER == 0
	, __global const PAYLOAD* brick1, __global const PAYLOAD* brick2, __global const PAYLOAD* brick3
#endif
)
{
	// produce primary ray for pixel
	const int x = get_global_id( 0 ), y = get_global_id( 1 );
#if GIRAYS == 0
	float4 pixel = render_whitted( (float2)(x, y), params, grid, BRICKPARAMS, sky, blueNoise, uberGrid );
#else
	float4 pixel = render_gi( (float2)(x, y), params, grid, BRICKPARAMS, sky, blueNoise, uberGrid );
#endif
	// finalize pixel
	write_imagef( outimg, (int2)(x, y), (float4)(LinearToSRGB( ToneMapFilmic_Hejl2015( pixel.xyz, 1 ) ), 1) );
}

// finalize: implementation of the Temporal Anti Aliasing algorithm.
__kernel void finalize( __global float4* prevFrameIn, __global float4* prevFrameOut,
	__global float4* frame, __constant struct RenderParams* params )
{
	const int x = get_global_id( 0 );
	const int y = get_global_id( 1 );
	// get history and current frame pixel - calculate u_prev, v_prev
	const float4 pixelData = frame[x + y * SCRWIDTH];
	const float2 uv = (float2)((float)x / (float)SCRWIDTH, (float)y / (float)SCRHEIGHT);
	float3 pixelPos = params->p0 + (params->p1 - params->p0) * uv.x + (params->p2 - params->p0) * uv.y;
	const float3 D = normalize( pixelPos - params->E );
	pixelPos = params->E + pixelData.w * D;
	const float dl = dot( pixelPos, params->Nleft.xyz ) - params->Nleft.w;
	const float dr = dot( pixelPos, params->Nright.xyz ) - params->Nright.w;
	const float dt = dot( pixelPos, params->Ntop.xyz ) - params->Ntop.w;
	const float db = dot( pixelPos, params->Nbottom.xyz ) - params->Nbottom.w;
	const float u_prev = SCRWIDTH * (dl / (dl + dr));
	const float v_prev = SCRHEIGHT * (dt / (dt + db));
	float3 hist = bilerpSample( prevFrameIn, u_prev, v_prev );
	const float3 pixel = RGBToYCoCg( pixelData.xyz );
	// determine color neighborhood
	int x1 = max( 0, x - 1 ), y1 = max( 0, y - 1 ), x2 = min( SCRWIDTH - 1, x + 1 ), y2 = min( SCRHEIGHT - 1, y + 1 );
	// advanced neighborhood determination
	float3 colorAvg = (float3)0;
	float3 colorVar = colorAvg;
	for (int y = y1; y <= y2; y++) for (int x = x1; x <= x2; x++)
	{
		const float3 p = RGBToYCoCg( frame[x + y * SCRWIDTH].xyz );
		colorAvg += p, colorVar += p * p;
	}
	colorAvg *= 1.0f / 9.0f;
	colorVar *= 1.0f / 9.0f;
	float3 sigma = max( (float3)0, colorVar - (colorAvg * colorAvg) );
	sigma.x = sqrt( sigma.x );
	sigma.y = sqrt( sigma.y );
	sigma.z = sqrt( sigma.z );
	const float3 minColor = colorAvg - 1.25f * sigma;
	const float3 maxColor = colorAvg + 1.25f * sigma;
	hist = clamp( RGBToYCoCg( hist ), minColor, maxColor );
	// final blend
	const float3 color = YCoCgToRGB( 0.9f * hist + 0.1f * pixel );
	prevFrameOut[x + y * SCRWIDTH] = (float4)(color, 1);
}

// unsharpen: this kernel finalizes the output of the TAA kernel. Unsharpen is
// applied because TAA tends to blur the screen; unsharpen (despite its name) remedies this.
__kernel void unsharpen( write_only image2d_t outimg, __global float4* pixels )
{
	const int x = get_global_id( 0 );
	const int y = get_global_id( 1 );
	if (x == 0 || y == 0 || x >= SCRWIDTH - 1 || y >= SCRHEIGHT - 1) return;
	const float4 p0 = pixels[x - 1 + (y - 1) * SCRWIDTH];
	const float4 p1 = pixels[x + (y - 1) * SCRWIDTH];
	const float4 p2 = pixels[x + 1 + (y - 1) * SCRWIDTH];
	const float4 p3 = pixels[x + 1 + y * SCRWIDTH];
	const float4 p4 = pixels[x + 1 + (y + 1) * SCRWIDTH];
	const float4 p5 = pixels[x + (y + 1) * SCRWIDTH];
	const float4 p6 = pixels[x - 1 + (y + 1) * SCRWIDTH];
	const float4 p7 = pixels[x - 1 + y * SCRWIDTH];
	const float4 color = max( pixels[x + y * SCRWIDTH], pixels[x + y * SCRWIDTH] *
		2.7f - 0.5f * (0.35f * p0 + 0.5f * p1 + 0.35f * p2 +
			0.5f * p3 + 0.35f * p4 + 0.5f * p5 + 0.35f * p6 + 0.5f * p7) );
	write_imagef( outimg, (int2)(x, y), (float4)(LinearToSRGB( ToneMapFilmic_Hejl2015( color.xyz, 1 ) ), 1) );
}

// traceBatch: trace a batch of rays to the first non-empty voxel.
__kernel void traceBatch(
	__read_only image3d_t grid,
	__global const PAYLOAD* brick0, __global const PAYLOAD* brick1,
	__global const PAYLOAD* brick2, __global const PAYLOAD* brick3,
	const int batchSize, __global const float4* rayData, __global uint* hitData,
	__global const unsigned char* uberGrid
)
{
	// sanity check
	const uint taskId = get_global_id( 0 );
	if (taskId >= batchSize) return;
	// fetch ray from buffer
	const float4 O4 = rayData[taskId * 2 + 0];
	const float4 D4 = rayData[taskId * 2 + 1];
	// trace ray
	float3 N;
	float dist;
	const uint voxel = TraceRay( (float4)(O4.x, O4.y, O4.z, 0), (float4)(D4.x, D4.y, D4.z, 1),
		&dist, &N, grid, uberGrid, BRICKPARAMS, 999999 );
	// store query result
	hitData[taskId * 2 + 0] = as_uint( dist < O4.w ? dist : 1e34f );
	uint Nval = ((int)N.x + 1) + (((int)N.y + 1) << 2) + (((int)N.z + 1) << 4);
	hitData[taskId * 2 + 1] = (voxel == 0 ? 0 : Nval) + (voxel << 16);
}

// traceBatchToVoid: trace a batch of rays to the first empty voxel.
// TODO: probably broken; needs to be synchronized witht the regular traversal kernel.
__kernel void traceBatchToVoid(
	__read_only image3d_t grid,
	__global const PAYLOAD* brick0, __global const PAYLOAD* brick1,
	__global const PAYLOAD* brick2, __global const PAYLOAD* brick3,
	const int batchSize, __global const float4* rayData, __global uint* hitData,
	__global const unsigned char* uberGrid
)
{
	// sanity check
	const uint taskId = get_global_id( 0 );
	if (taskId >= batchSize) return;
	// fetch ray from buffer
	const float4 O4 = rayData[taskId * 2 + 0];
	const float4 D4 = rayData[taskId * 2 + 1];
	// trace ray
	float3 N;
	float dist;
	TraceRayToVoid( (float4)(O4.x, O4.y, O4.z, 0), (float4)(D4.x, D4.y, D4.z, 1),
		&dist, &N, grid, BRICKPARAMS, uberGrid );
	// store query result
	hitData[taskId * 2 + 0] = as_uint( dist < O4.w ? dist : 1e34f );
	uint Nval = ((int)N.x + 1) + (((int)N.y + 1) << 2) + (((int)N.z + 1) << 4);
	hitData[taskId * 2 + 1] = Nval;
}

// commit: this kernel moves changed bricks which have been transfered to the on-device
// staging buffer to their final location.
__kernel void commit( const int taskCount, __global uint* staging,
	__global uint* brick0, __global uint* brick1, __global uint* brick2, __global uint* brick3 )
{
	// put bricks in place
	int task = get_global_id( 0 );
	if (task < taskCount)
	{
	#if ONEBRICKBUFFER == 0
		__global uint* bricks[4] = { brick0, brick1, brick2, brick3 };
	#endif
		int brickId = staging[task + GRIDSIZE];
		__global uint* src = staging + MAXCOMMITS + GRIDSIZE + task * (BRICKSIZE * PAYLOADSIZE) / 4;
		const uint offset = brickId * BRICKSIZE * PAYLOADSIZE / 4; // in dwords
	#if ONEBRICKBUFFER == 1
	#if MORTONBRICKS == 1
		__global PAYLOAD* dst = (__global PAYLOAD*)(brick0 + offset);
		for (int z = 0; z < 8; z++) for (int y = 0; y < 8; y++) for (int x = 0; x < 8; x++)
			dst[Morton3Bit( x, y, z )] = ((__global PAYLOAD*)src)[x + y * 8 + z * 64];
	#else
		for (int i = 0; i < (BRICKSIZE * PAYLOADSIZE) / 4; i++) brick0[offset + i] = src[i];
	#endif
	#else
		__global uint* page = bricks[(offset / (CHUNKSIZE / 4)) & 3];
		for (int i = 0; i < (BRICKSIZE * PAYLOADSIZE) / 4; i++) page[(offset & (CHUNKSIZE / 4 - 1)) + i] = src[i];
	#endif
	}
}

#if MORTONBRICKS == 1
// encodeBricks: this kernel shuffles the voxels in a brick to Morton order.
// On NVidia, despite more coherent memory access, this is significantly slower.
__kernel void encodeBricks( const int taskCount, __global PAYLOAD* brick )
{
	// change brick layout to morton order
	int task = get_global_id( 0 );
	if (task < taskCount)
	{
		PAYLOAD tmp[512];
		for (int i = 0; i < 512; i++) tmp[i] = brick[task * 512 + i];
		for (int z = 0; z < 8; z++) for (int y = 0; y < 8; y++) for (int x = 0; x < 8; x++)
			brick[task * 512 + Morton3Bit( x, y, z )] = tmp[x + y * 8 + z * 64];
	}
}
#endif

// updateUberGrid: this kernel creates the 32x32x32 'ubergrid', which contains a '0' for
// a group of empty 4x4x4 bricks; '1' otherwise.
__kernel void updateUberGrid( const __global unsigned int* grid, __global unsigned char* uber )
{
	const int task = get_global_id( 0 );
	const int x = task & (UBERWIDTH - 1);
	const int z = (task / UBERWIDTH) & (UBERDEPTH - 1);
	const int y = (task / (UBERWIDTH * UBERHEIGHT)) & (UBERHEIGHT - 1);
	// check grid
	bool empty = true;
	for (int a = 0; a < 4; a++) for (int b = 0; b < 4; b++) for (int c = 0; c < 4; c++)
	{
		const int gx = x * 4 + a, gy = y * 4 + b, gz = z * 4 + c;
		if (grid[gx + gz * GRIDWIDTH + gy * GRIDWIDTH * GRIDHEIGHT]) { empty = false; break; }
	}
	// write result
	uber[x + z * UBERWIDTH + y * UBERWIDTH * UBERHEIGHT] = empty ? 0 : 1;
}


 