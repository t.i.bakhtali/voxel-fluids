#include "precomp.h"
#include "waterworld.h"

Game* CreateGame() { return new WaterWorld(); }

void WaterWorld::SetStaticBlock(uint x0, uint y0, uint z0, uint w, uint h, uint d, uint v)
{
	if (x0 + w > MAPWIDTH || y0 + h > MAPWIDTH || z0 + d > MAPWIDTH)
	{
		cout << "Block outside of grid" << endl;
		return;
	}
	for (uint x = x0; x < x0 + w; x++)
		for (uint y = y0; y < y0 + h; y++)
			for (uint z = z0; z < z0 + d; z++)
				Plot(x, y, z, v);
}

//water dropped from a large height, system should only maintain 
//and update area with water in it (e.g. alive bricks don't keep increasing, and
//decrease once the water compresses into the box
void WaterWorld::InitialiseWaterDropScenario()
{
	SetMaterialBlock(480, 8, 504, 48, 4, 24, 1, false);
	SetMaterialBlock(520, 256, 504, 8, 16, 8, 1, false);

	SetStaticBlock(480, 8, 503, 48, 16, 1, WHITE);
	SetStaticBlock(528, 8, 504, 1, 16, 24, WHITE);
	SetStaticBlock(480, 8, 528, 48, 16, 1, WHITE);
	SetStaticBlock(479, 8, 504, 1, 16, 24, WHITE);
}

//Little dam holding water with a hole in it
void WaterWorld::InitialiseDamHoleScenario()
{
	SetMaterialBlock(520, 8, 504, 8, 16, 24, 1, false);

	//Dam divider
	SetStaticBlock(519, 8, 504, 1, 16, 24, WHITE);
	SetStaticBlock(519, 10, 512, 1, 6, 2, 0);

	SetStaticBlock(480, 8, 503, 48, 16, 1, WHITE);
	SetStaticBlock(528, 8, 504, 1, 16, 24, WHITE);
	SetStaticBlock(480, 8, 528, 48, 16, 1, WHITE);
	SetStaticBlock(479, 8, 504, 1, 16, 24, WHITE);

}

//Spawns a wall of water that will collapse
void WaterWorld::InitialiseDamBreakScenario()
{
	SetMaterialBlock(520, 8, 504, 8, 16, 24, 1, false);

	SetStaticBlock(480, 8, 503, 48, 16, 1, WHITE);
	SetStaticBlock(528, 8, 504, 1, 16, 24, WHITE);
	SetStaticBlock(480, 8, 528, 48, 16, 1, WHITE);
	SetStaticBlock(479, 8, 504, 1, 16, 24, WHITE);
}

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------
void WaterWorld::Init()
{
	ShowCursor( false );
	skyDomeLightScale = 6.0f;
	// default scene is a box; punch a hole in the ceiling
	Box( 256, 240, 256, 768, 260, 768, 0 );
	// add some objects
	//Outerbox
	SetStaticBlock(400, 7, 400, 200, 1, 200, WHITE);
	SetStaticBlock(400, 8, 600, 200, 8, 1, WHITE);
	SetStaticBlock(600, 8, 400, 1, 8, 200, WHITE);
	SetStaticBlock(400, 8, 400, 200, 8, 1, WHITE);
	SetStaticBlock(400, 8, 400, 1, 8, 200, WHITE);

	InitCAPE(100);

	//A few scenario's to choose from
	//InitialiseWaterDropScenario();
	InitialiseDamHoleScenario();
	//InitialiseDamBreakScenario();
}

// -----------------------------------------------------------
// HandleInput: reusable input handling / free camera
// -----------------------------------------------------------
void WaterWorld::HandleInput( float deltaTime )
{
	// free cam controls
	float3 tmp( 0, 1, 0 ), right = normalize( cross( tmp, D ) ), up = cross( D, right );
	float speed = deltaTime * 0.1f;
	if (GetAsyncKeyState( 'W' )) O += speed * D; else if (GetAsyncKeyState( 'S' )) O -= speed * D;
	if (GetAsyncKeyState( 'A' )) O -= speed * right; else if (GetAsyncKeyState( 'D' )) O += speed * right;
	if (GetAsyncKeyState( 'R' )) O += speed * up; else if (GetAsyncKeyState( 'F' )) O -= speed * up;
	if (GetAsyncKeyState( VK_LEFT )) D = normalize( D - right * 0.025f * speed );
	if (GetAsyncKeyState( VK_RIGHT )) D = normalize( D + right * 0.025f * speed );
	if (GetAsyncKeyState( VK_UP )) D = normalize( D - up * 0.025f * speed );
	if (GetAsyncKeyState( VK_DOWN )) D = normalize( D + up * 0.025f * speed );
#if 1
	// enable to set spline path points using P key
	static bool pdown = false;
	static FILE* pf = 0;
	if (!GetAsyncKeyState( 'P' )) pdown = false; else
	{
		if (!pdown) // save a point for the spline
		{
			if (!pf) pf = fopen( "spline.bin", "wb" );
			fwrite( &O, 1, sizeof( O ), pf );
			float3 t = O + D;
			fwrite( &t, 1, sizeof( t ), pf );
		}
		pdown = true;
	}
	LookAt( O, O + D );
#else
	// playback of recorded spline path
	const size_t N = splinePath.size();
	PathPoint p0 = splinePath[(pathPt + (N - 1)) % N], p1 = splinePath[pathPt];
	PathPoint p2 = splinePath[(pathPt + 1) % N], p3 = splinePath[(pathPt + 2) % N];
	LookAt( CatmullRom( p0.O, p1.O, p2.O, p3.O ), CatmullRom( p0.D, p1.D, p2.D, p3.D ) );
	if ((t += deltaTime * 0.0005f) > 1) t -= 1, pathPt = (pathPt + 1) % N;
#endif
}

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------
void WaterWorld::Tick( float deltaTime )
{
	// update camera
	HandleInput( deltaTime );

	UpdateCAPE(deltaTime);
}